package net.blockcade.testing.Commands;

import net.blockcade.testing.Utils.Text;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GamemodeCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {
        if(!(sender instanceof Player)){
            // Non-player executed the command (Console)
            sender.sendMessage("That command may only be executed by a player");
            return false;
        }
        // Define player object (Used to get player data)
        Player player = (Player)sender;

        if(args.length==0){
            // No arguments specified
            sender.sendMessage(Text.format("&aYour GameMode is currently %s",player.getGameMode().name()));
            return true;
        }

        // Check for requested gamemode
        switch(args[0].toUpperCase()){
            case "CREATIVE":
            case "C":
                // Requested creative
                player.setGameMode(GameMode.CREATIVE);
                break;

            case "SURVIVAL":
            case "S":
                // Requested survival
                player.setGameMode(GameMode.SURVIVAL);
                break;

            case "ADVENTURE":
            case "A":
                // Requested adventure
                player.setGameMode(GameMode.ADVENTURE);
                break;

            case "SPECTATOR":
            case "SP":
                // Requested spectator
                player.setGameMode(GameMode.SPECTATOR);
                break;
        }

        sender.sendMessage(Text.format("&aYour GameMode is now %s",player.getGameMode().name()));
        return true;
    }
}
