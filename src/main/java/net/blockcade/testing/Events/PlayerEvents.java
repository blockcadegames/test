package net.blockcade.testing.Events;

import net.blockcade.testing.Utils.Text;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class PlayerEvents implements Listener {

    @EventHandler
    public void PlayerJoin(PlayerJoinEvent event){

        Player player = event.getPlayer();

        // Send welcome message
        player.sendMessage(Text.formatAll(
                "&c&m-=============-&r",
                "&a    Welcome "+player.getDisplayName()+"&r",
                "&c&m-=============-&r"
        ));

        // Give Diamond Sword
        ItemStack DIAMOND_SWORD_IS = new ItemStack(Material.DIAMOND_SWORD,1);
        ItemMeta DIAMOND_SWORD_META = DIAMOND_SWORD_IS.getItemMeta();

        /*
         * NOTE: Java makes all numbers start from 0 meaning Knockback 1 would be Knockback 0 in the code
         */
        DIAMOND_SWORD_META.addEnchant(Enchantment.KNOCKBACK,1,true);

        DIAMOND_SWORD_META.setDisplayName(Text.format("&cSword&r"));
        DIAMOND_SWORD_META.setLore(Text.formatList("Howdy"));

        DIAMOND_SWORD_IS.setItemMeta(DIAMOND_SWORD_META);

        player.getInventory().addItem(DIAMOND_SWORD_IS);
    }

}
