package net.blockcade.testing;

import net.blockcade.testing.Commands.GamemodeCommand;
import net.blockcade.testing.Events.PlayerEvents;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;

public class Main extends JavaPlugin {

    @Override
    public void onEnable() {

        // Plugin Manager
        PluginManager pm = Bukkit.getPluginManager();

        // Register Commands & Check the command was defined in plugin.yml
        Objects.requireNonNull(getCommand("gamemode")).setExecutor(new GamemodeCommand());

        // Register Events
        pm.registerEvents(new PlayerEvents(),this);
    }

}
