package net.blockcade.testing.Utils;

import com.google.common.collect.Lists;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Formatter;
import java.util.List;

public class Text {

    public static String format(String str){
        return ChatColor.translateAlternateColorCodes('&',str);
    }
    public static String format(String str, String... args) {
        return ChatColor.translateAlternateColorCodes('&', "&r" + (new Formatter().format(str, args)));
    }
    public static String[] formatAll(String... str){
        List<String> stringList = new ArrayList<>();
        for(String s : str)
            stringList.add(format(s));
        return stringList.toArray(new String[]{});
    }
    public static List<String> formatList(String... str){
        List<String> stringList = new ArrayList<>();
        for(String s : str)
            stringList.add(format(s));
        return stringList;
    }

}
